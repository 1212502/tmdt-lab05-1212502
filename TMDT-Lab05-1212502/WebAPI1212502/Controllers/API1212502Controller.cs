﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI1212502.Models;

namespace WebAPI1212502.Controllers
{
    public class API1212502Controller : ApiController
    {
        public static Danhsach Ds = new Danhsach();
        [Authorize]
        [HttpGet]
        [Route("api/2.0/users/{username}")]
        public IHttpActionResult getInfor1212502(string username)
        {
            Users temp;
            if ((temp = Ds.GetUser(username)) != null)
            {
                return Ok(temp);
            }
            else
                return NotFound();
        }
        [Authorize]
        [HttpGet]
        [Route("api/2.0/users")]
        public IHttpActionResult getALL1212502()
        {
            List<Users> all = Ds.GetAll();
            if(all.Count == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(all);
            }
        }
        [Authorize]
        [HttpPost]
        [Route("api/2.0/users")]
        public IHttpActionResult addUsers1212502([FromBody]Users a)
        {
            if (Ds.Addlist(a))
            {
                return Ok();
            }
            else
                return NotFound();
        }
        [Authorize]
        [HttpDelete]
        [Route("api/2.0/users/{username}")]
        public IHttpActionResult deleteUsers1212502(string username)
        {
            if (Ds.Deleteuser(username))
            {
                return Ok();
            }
            else
                return NotFound();
        }
        [Authorize]
        [HttpPut]
        [Route("api/2.0/users/{username}")]
        public IHttpActionResult updateUsers1212502(string username, [FromBody]Users a)
        {
            if (Ds.Update(username, a))
            {
                return Ok();
            }
            else
                return NotFound();
        }
        [Authorize]
        [HttpGet]
        [Route("api/2.0/products")]
        public IHttpActionResult searchProduct1212502([FromUri] string category)
        {
            List<Product> t = Ds.SearchCategory(category);
            if (t.Count == 0)
                return NotFound();
            return Ok(t);
        }
        [Authorize]
        [HttpGet]
        [Route("api/2.0/products/{category}/{username}")]
        public IHttpActionResult searchkethop1212502([FromUri] string category, [FromUri]string username)
        {
            List<Product> t = Ds.SearchCategoryUser(category, username);
            if (t.Count == 0)
                return NotFound();
            return Ok(t);
        }

    }
}
