﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI1212502.Models
{
    public class Danhsach
    {
        private List<Users> Listuser;
        private List<Product> Listproduct;
        public Danhsach()
        {
            Listuser = new List<Users>();
            Listuser.Add(new Users() { Username = "1212502", Hovaten = "Nguyễn Văn Ty" });
            Listuser.Add(new Users() { Username = "1212508", Hovaten = "Trần Văn Việt" });
            Listuser.Add(new Users() { Username = "1212756", Hovaten = "Trần Văn Hậu" });
            Listproduct = new List<Product>();
            Listproduct.Add(new Product() { MaSP = 1, NameProduct = "Samsung galaxy s5", Category = "Điện thoại", username = "1212502" });
            Listproduct.Add(new Product() { MaSP = 2, NameProduct = "Samsung galaxy s6", Category = "Điện thoại", username = "1212508" });
            Listproduct.Add(new Product() { MaSP = 3, NameProduct = "Lumia 520", Category = "Điện thoại", username = "1212502" });
            Listproduct.Add(new Product() { MaSP = 4, NameProduct = "Iphone 6", Category = "Điện thoại", username = "1212502" });
            Listproduct.Add(new Product() { MaSP = 5, NameProduct = "Asus 520", Category = "Laptop", username = "1212508" });
            Listproduct.Add(new Product() { MaSP = 6, NameProduct = "Dell", Category = "Laptop", username = "1212502" });
            Listproduct.Add(new Product() { MaSP = 7, NameProduct = "HP", Category = "Laptop", username = "1212502" });
            Listproduct.Add(new Product() { MaSP = 8, NameProduct = "Canon", Category = "USB", username = "1212508" });
            Listproduct.Add(new Product() { MaSP = 9, NameProduct = "kingston", Category = "USB", username = "1212502" });
            Listproduct.Add(new Product() { MaSP = 10, NameProduct = "Toshiba", Category = "USB", username = "1212502" });
        }
        public bool Addlist(Users a)
        {
            if (Containt(a.Username) == null)
            {
                Listuser.Add(a);
                return true;
            }
            return false;
        }
        public List<Users> GetAll()
        {
            return Listuser;
        }
        public Users GetUser(string username)
        {
            foreach(var temp in Listuser)
            {
                if(temp.Username == username)
                {
                    return temp;
                }
            }
            return null;
        }
        public bool Deleteuser(string username)
        {
            foreach (var temp in Listuser)
            {
                if (temp.Username == username)
                {
                    Listuser.Remove(temp);
                    return true;
                }
            }
            return false;
        }
        public bool Update(string id,Users a)
        {
            Users temp;
            for(int i = 0 ; i < Listuser.Count ; i++)
            {
                if(Listuser[i].Username == a.Username)
                {
                    Listuser[i] = a;
                    return true;
                }
            }
            return false;
        }
        private Users Containt(string username)
        {
            foreach (var temp in Listuser)
            {
                if (temp.Username == username)
                {
                    return temp;
                }
            }
            return null;
        }
        public List<Product> SearchCategory(string cate)
        {
            List<Product> temp = new List<Product>();
            foreach(var t in Listproduct)
            {
                if(t.Category == cate)
                {
                    temp.Add(t);
                }
            }
            return temp;
        }
        public List<Product> SearchCategoryUser(string cate,string usename)
        {
            List<Product> temp = new List<Product>();
            foreach (var t in Listproduct)
            {
                if (t.Category == cate && t.username == usename)
                {
                    temp.Add(t);
                }
            }
            return temp;
        }
    }
}