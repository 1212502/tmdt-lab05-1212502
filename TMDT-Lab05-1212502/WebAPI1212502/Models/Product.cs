﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI1212502.Models
{
    public class Product
    {
        public int MaSP { get; set; }
        public string NameProduct { get; set; }
        public string Category { get; set; }
        public string username { get; set; }
    }
}