﻿using MVCWebAPIClient1212502.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace MVCWebAPIClient1212502.Controllers
{
    public class HomeController : Controller
    {
        private static string Token;
        private static string username = "1212502";
        private static string password = "123456";
        // GET: Home
        public ActionResult Index()
        {
            if (Token == null)
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:15865/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    // truyen theo dang url
                    var content = new FormUrlEncodedContent(new[] 
                    {
                        new KeyValuePair<string, string>("grant_type", "password"),
                        new KeyValuePair<string, string>("username",username),
                        new KeyValuePair<string, string>("password",password)
                    });
                    HttpResponseMessage respon = client.PostAsync("token", content).Result;
                    if (respon.IsSuccessStatusCode)
                    {
                        string resultContent = respon.Content.ReadAsStringAsync().Result;
                        dynamic temp = Newtonsoft.Json.JsonConvert.DeserializeObject(resultContent);
                        Token = temp.access_token;
                    }
                    else
                        ViewBag.error = "Tai khoan khong dung";

                }
            }
            return View();
        }
        [HttpGet]
        public ActionResult GetUsers(string value)
        {
            Users temp = new Users();
            using(var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:15865/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",Token);
                string url = "api/2.0/users/" + value;
                HttpResponseMessage repons = client.GetAsync(url).Result;
                
                if(repons.IsSuccessStatusCode)
                {
                    temp = repons.Content.ReadAsAsync<Users>().Result;
                }
            }
            return View(temp);
        }
        [HttpGet]
        public ActionResult GetAllUsers()
        {
            List<Users> temp = new List<Users>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:15865/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                string url = "api/2.0/users";
                HttpResponseMessage repons = client.GetAsync(url).Result;

                if (repons.IsSuccessStatusCode)
                {
                    temp = repons.Content.ReadAsAsync<List<Users>>().Result;
                }
            }
            return View(temp);
        }
        public ActionResult ThaotacUsers(string notifi,Users use)
        {
            if (use == null)
                use = new Users();
            ViewBag.notifi = notifi;
            return View(use);
        }
        [HttpPost]
        public ActionResult Themmoi(Users a)
        {
            if(a.Hovaten == "" || a.Hovaten == null)
            {
                return RedirectToAction("ThaotacUsers", new { notifi = "Bạn Chưa nhập tên"});
            }
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:15865/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                string url = "api/2.0/users";
                var model = new
                {
                    Username = a.Username,
                    Hovaten = a.Hovaten
                };
                HttpResponseMessage repons = client.PostAsJsonAsync(url,model).Result;

                if (repons.IsSuccessStatusCode)
                {
                    return RedirectToAction("ThaotacUsers", new { notifi = "Thêm mới thành công"});
                }
            }
            return RedirectToAction("ThaotacUsers", new { notifi = "Thêm mới thất bại( hãy kiểm tra lại"});
        }
        [HttpPost]
        public ActionResult Delete(Users a)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:15865/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                string url = "api/2.0/users/" + a.Username;
                
                HttpResponseMessage repons = client.DeleteAsync(url).Result;

                if (repons.IsSuccessStatusCode)
                {
                    return RedirectToAction("ThaotacUsers", new { notifi = "Xóa thành công"});
                }
            }
            return RedirectToAction("ThaotacUsers", new { notifi = "Username không tồn tại"});
        }
        [HttpPost]
        public ActionResult Update(Users a)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:15865/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                string url = "api/2.0/users/" + a.Username;
                var model = new
                {
                    Username = a.Username,
                    Hovaten = a.Hovaten
                };
                HttpResponseMessage repons = client.PutAsJsonAsync(url, model).Result;

                if (repons.IsSuccessStatusCode)
                {
                    return RedirectToAction("ThaotacUsers", new { notifi = "Update thành công" });
                }
            }
            return RedirectToAction("ThaotacUsers", new { notifi = "Update thất bại( hãy kiểm tra lại" });
        }
        public ActionResult Timkiemsanpham()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Timkiemproduct(string cate,string use)
        {
            List<Product> temp = new List<Product>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:15865/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

                if(use == "")
                {
                    string url = "api/2.0/products?category=" + cate;
                    HttpResponseMessage repons = client.GetAsync(url).Result;

                    if (repons.IsSuccessStatusCode)
                    {
                        temp = repons.Content.ReadAsAsync<List<Product>>().Result;
                    }
                }
                else
                {
                    string url = "api/2.0/products/" + cate + "/" + use;
                    HttpResponseMessage repons = client.GetAsync(url).Result;

                    if (repons.IsSuccessStatusCode)
                    {
                        temp = repons.Content.ReadAsAsync<List<Product>>().Result;
                    }
                }
            }
            return View(temp);
        }
    }
}